﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TeamPOINTless.Areas.Admin.Controllers;
using TeamPOINTless.Areas.Admin.Models;
using TeamPOINTless.Tests.StubsMocksFakes;

namespace TeamPOINTless.Tests.Areas.Admin.Controllers
{
    [TestClass]
    public class SubjectControllerTest
    {
   //     private readonly ISubjectRepository _subjectRepository;
        private SubjectController sut;

        [TestInitialize()]
        public void Initialize()
        {
            // Arrange
            sut = new SubjectController(new StubSubjectRepository());
        }

        [TestCleanup()]
        public void Cleanup()
        {
            sut = null;
        }

        [TestMethod]
        public void TestThatWeCanCreateASubjectController()
        {
            // Assert
            Assert.IsNotNull(sut, "Should not be null");
        }

        [TestMethod]
        public void TestThatWhenIndexIsCalledItRedirectsToActionNamedContentInControllerContent()
        {
            // Act
            var result = sut.Index() as RedirectToRouteResult;

            // Assert
            Assert.IsNotNull(result, "Not a redirect result. Should not be null");
            Assert.AreEqual("Content", result.RouteValues["Action"], "Should be Cotent");
            Assert.AreEqual("Content", result.RouteValues["Controller"], "Should be Cotent");
        }    
        
        [TestMethod]
        public void TestThatWhenDetailsIsCalledWithIdASubjectIsReturned()
        {
            // Act
            var result = sut.Details(1) as ViewResult;

            // Assert
            Assert.IsNotNull(result, "Should return a subject");
            Assert.IsInstanceOfType(result.Model, new Subject().GetType(), "Should be a instance of Subject");
        }
    }
}
