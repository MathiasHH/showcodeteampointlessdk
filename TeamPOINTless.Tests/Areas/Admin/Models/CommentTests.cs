﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TeamPOINTless.Areas.Admin.Models;

namespace TeamPOINTless.Tests.Areas.Admin.Models
{
    [TestClass]
    public class CommentTests
    {
        Comment sut;

        [TestInitialize()]
        public void Initialize()
        {
            //Arrange
            sut = new Comment()
            {
                Id = 1,
                PageId = 2,
                Position = 1,
                IsApprovedAndThereforeVisible = true,
                Content = "Utilfreds med kommende løb",
            };
        }

        [TestCleanup()]
        public void Cleanup()
        {
            sut = null;
        }

        [TestMethod]
        public void TestThatWeCanCreateAComment()
        {
            Assert.IsNotNull(sut, "Should not be null");
        }

        [TestMethod]
        public void TestThatWeCanCreateACommentWithWantedFields()
        {
            Assert.IsNotNull(sut, "Should not be null");
            Assert.AreEqual(1, sut.Id, "Comment id should be 1");
            Assert.AreEqual(2, sut.PageId, "Comment pageId should be 2");
            Assert.AreEqual(1, sut.Position, "Position should be 1");
            Assert.AreEqual(true, sut.IsApprovedAndThereforeVisible, "IsApprovedAndThereforeVisible should be true");
            Assert.AreEqual("Utilfreds med kommende løb", sut.Content, "Content should be Utilfreds med kommende løb");
        }



    }
}
