﻿using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TeamPOINTless.Areas.Admin.Models;

namespace TeamPOINTless.Tests.Areas.Admin.Models
{
    [TestClass]
    public class EditUserViewModelTest
    {
        private EditUserViewModel sut;

        [TestInitialize()]
        public void Initialize()
        {
            //Arrange
            sut = new EditUserViewModel()
            {
                Id = "1",
                EmailConfirmed = true, 
            };
        }

        [TestCleanup()]
        public void Cleanup()
        {
            sut = null;
        }

        [TestMethod]
        public void TestThatWeCanCreateAEditUserViewModel()
        {
            Assert.IsNotNull(sut, "Should not be null");
        }   
        
        [TestMethod]
        public void TestThatWeCanCreateAEditUserViewModelWithWantedFields()
        {
            Assert.AreEqual("1", sut.Id, "EditUserViewModel id should be 1");
            Assert.IsTrue(sut.EmailConfirmed, "Should be true");
            Assert.IsInstanceOfType(sut.EmailConfirmed, typeof(bool), "Sut.id should be a bool");
           
        }
        //Todo why is id a string
        [TestMethod]
        public void TestThatIdIsAString()
        {
            Assert.IsTrue(sut.Id is string, "Id should be of type string"); //why?
        }    
    }
}
