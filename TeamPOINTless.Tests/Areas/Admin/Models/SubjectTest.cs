﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.NetworkInformation;
using System.Web.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TeamPOINTless.Areas.Admin.Models;

namespace TeamPOINTless.Tests.Areas.Admin.Models
{
    [TestClass]
    public class SubjectTest
    {
        [TestMethod]
        public void TestThatWeCanCreateASubject()
        {
            //Arrange
            Subject subject = new Subject();
            //Assert
            Assert.IsNotNull(subject, "Should not be null");
        }   
        
        [TestMethod]
        public void TestThatWeCanCreateASubjectWithWantedFields_ClassicalStyle()
        {
            //Arrange
            Subject subject = new Subject()
            {
                ID = 1,
                Title = "Kommende løb",
                Position = 1,
                //Some might argue for we should use mocks for dependecy to only test the class and not the dependency.
                Visible = VisibleEnum.NotVisible, 
                Pages = new Collection<Page>() { new Page(){}, new Page() { Title = "En ny side"} },
            };
            //Assert
            Assert.IsNotNull(subject, "Should not be null");
            Assert.AreEqual(1, subject.ID, "Subject id should be 1");
        }     
        [TestMethod]
        public void TestThatWeCanCreateASubjectWithWantedFields_MockistStyle()
        {
           var Pages1 =  new Mock<ICollection<Page>>();
//           var Pages11 =  new Mock<ICollection<Mock<Page>>>();
//            Pages11.Setup(x => x.Add(new Mock<Page>(){}));
           //            var aPage = new Mock<Page>() {};
//            Pages11.Setup(x => x.Add(aPage));

            //Arrange
            Subject subject = new Subject()
            {
                ID = 1,
                Title = "Kommende løb",
                Position = 1,
                //Some might argue for we should use mocks for dependecy to only test the class and not the dependency.
                Visible = VisibleEnum.NotVisible,
                Pages = Pages1.Object
//                Pages = new Collection<Page>() { new Page() {}, new Page() {} },
            };
            //Assert
            Assert.IsNotNull(subject, "Should not be null");
            Assert.AreEqual(1, subject.ID, "Subject id should be 1");
        }
    }
}
