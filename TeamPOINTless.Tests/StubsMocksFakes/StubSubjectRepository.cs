﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamPOINTless.Areas.Admin.Models;

namespace TeamPOINTless.Tests.StubsMocksFakes
{
    public class StubSubjectRepository : ISubjectRepository
    {
        public Subject FindById(int? id)
        {
            return new Subject(){ID = 1, Title = "Kommende løb"};//What subject contains dosnt matter. We test the controller here not the Model.
        }
    }
}
