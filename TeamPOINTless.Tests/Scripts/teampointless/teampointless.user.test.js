///<reference path="../../../TeamPOINTless/Scripts/teampointless/teampointless.user.js"/>

/**
 * Created by mhh on 12/01/15.
 */
/* Asserts - https://api.qunitjs.com/category/assert/ */

(function () {
    'use strict';

    module("Test");
        QUnit.test("This test pass", function () {
            ok(myMax.max1(1, 2) === 2);
        });

//        QUnit.test("testtest tester la returns 42", function () {
//            var te = new Tester();
//            var v = jQuery('#email-confirmed');
//            alert(JSON.stringify(v));
//            deepEqual(te.la(), 42);
//        });
//    
        QUnit.test("Should have a hide method", function () {
            // Arrange
            var user = new User(true);
    
            //Act
            var result = user.hideIfEmailIsConfirmed();
    
            // Assert
            ok(result);
        });


//    QUnit.test("assert.async() test", function () {
//        //        // Arrange
//        var emailconfirmed = true;
//        var user = new User(emailconfirmed);
//        $.when(
//            user.hideIfEmailIsConfirmed()
//        ).done(function() {
//            var emailConfirmedElementIsHidden = $('#email-confirmed').is(":hidden");
//            ok(emailConfirmedElementIsHidden, "Should be true12");
//        });
//    });

    /*
    https://github.com/jquery/jquery/blob/master/test/unit/effects.js
    1577

 * test( "hide called on element within hidden parent should set display to none (#10045)", 3, function() {
var hidden = jQuery(".hidden"),
elems = jQuery("<div>hide</div><div>hide0</div><div>hide1</div>");
hidden.append( elems );
jQuery.when(
elems.eq( 0 ).hide(),
elems.eq( 1 ).hide( 0 ),
elems.eq( 2 ).hide( 1 )
).done(function() {
strictEqual( elems.get( 0 ).style.display, "none", "hide() called on element within hidden parent should set display to none" );
strictEqual( elems.get( 1 ).style.display, "none", "hide( 0 ) called on element within hidden parent should set display to none" );
strictEqual( elems.get( 2 ).style.display, "none", "hide( 1 ) called on element within hidden parent should set display to none" );
elems.remove();
});
this.clock.tick( 10 );
});
 */

})();

