﻿///<reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/qunit/qunit.d.ts"/>
/// <reference path="../../../Teampointless/Scripts/teampointless/teampointless.user.ts"/>
(function () {
    //       This is broken shit
    //
    //    var user;
    //    QUnit.module("module - Test User. On", {
    //        beforeEach: () => {
    //            // prepare something for all following tests
    //            user = new UserTester("");
    //        },
    //        afterEach: function () {
    //            // clean up after each test
    //            user = null;
    //        }
    //    });
    QUnit.test("Should be able to create a User object", function () {
        // Arrange & Act
        var user = new User(true);

        // Assert
        ok(user !== null);
    });
    QUnit.test("emailConfirmed should be false", function () {
        //Arrange & Act
        var user = new User(false);

        // we use any to break encapsulation for unit testing private stuff
        //Assert
        deepEqual(user.emailConfirmed, false);
    });
    QUnit.test("emailConfirmed should be true", function () {
        //Arrange & Act
        var user = new User(true);

        // we use any to break encapsulation for unit testing private stuff
        //Assert
        deepEqual(user.emailConfirmed, true);
    });

    QUnit.test("We should be able to call hideIfEmailIsConfirmed", function () {
        //Arrange & Act
        var user = new User(false);

        //Act
        var actual = user.hideIfEmailIsConfirmed();

        //Assert
        deepEqual(actual, true);
    });

    QUnit.test(" virker ikke", function () {
        //Arrange & Act
        var user = new User(false);

        //Act
        user.hideIfEmailIsConfirmed();

        jQuery('#email-confirmed').hide(0, "swing", null);

        var actual = jQuery('#email-confirmed').is(":hidden");

        //Assert
        deepEqual(actual, false);
    });
})();
