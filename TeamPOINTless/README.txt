﻿# NoteToSelf
This project was my first experience with asp.net mvc 5. I was learning it while I was building the project. So I kind of hacked my way to completion. There is no test, code to abstraction/interface and all is concrete classes. I tried to keep it simple. Next time i think i will try to do tdd and have integration test and some end2end tests. So in this project there is a job of testing the code, unit/integration test ect. I could have decided to have many spikes that would make me capable of code to abstraction and do tdd, but i decided i wanted to hack around and move forward fast. So in a way it's kind of a big spike that I can look at next time I do a Asp.net MVC project. Even though I want to to tdd next time I want to look into how you can set up visual studio/a project so I can edit serverside code while the project is running. It will make it faster when you create a spike for new stuff. So look into how you do that, and what's possibly and not (Linq, DB access?).


Difference between develop and production
Webconfig: 
	Bundling and minification debug true for develop false for prod. - See http://www.asp.net/mvc/overview/performance/bundling-and-minification
		<system.web><compilation debug="false"/></system.web>
	
When publish remember to set debug or prod.

Anti-forgery-token
http://stackoverflow.com/questions/18097401/the-anti-forgery-cookie-token-and-form-field-token-do-not-match-in-mvc-4


#find version of asp.net mvc - var v = typeof (Controller).Assembly.GetName().Version;

MVVM
http://sampathloku.blogspot.dk/2012/10/how-to-use-viewmodel-with-aspnet-mvc.html

#Nuget
	https://docs.nuget.org/consume/package-manager-console

#Date
There is a bug in chrome
http://www.asp.net/mvc/overview/getting-started/introduction/examining-the-edit-methods-and-edit-view
#Sprog / Internalization
http://afana.me/post/aspnet-mvc-internationalization.aspx

Datetimepicker
https://github.com/Eonasdan/bootstrap-datetimepicker/wiki/Installation
http://eonasdan.github.io/bootstrap-datetimepicker/#using-locales
Remember :  <globalization   culture="da-DK" uiCulture="da-DK" /> in Web.config

#JavaScript and Razor
	- mix javascript inside razor : <script>alert("@ViewBag.AlertMessage");</script>
        /*var userAuthorized = @User.Identity.IsAuthenticated.ToString().ToLower();*/ 
	- how to set a select box in Razor : $('#visiblevalue option[value="' + selectedValue + '"]').prop('selected', true);
	


#What does this mean?
	db.Entry(page).State = EntityState.Modified;


#WYSIWYG
http://www.codeproject.com/Articles/830925/Integrating-TinyMCE-into-an-MVC-Project

#pics
dropbox public
http://www.picresize.com


#Security

1) http://www.asp.net/mvc/overview/security/create-an-aspnet-mvc-5-app-with-facebook-and-google-oauth2-and-openid-sign-on#ssl
http://typecastexception.com/post/2014/04/06/ASPNET-MVC-Keep-Private-Settings-Out-of-Source-Control.aspx

#login
http://stackoverflow.com/questions/20424704/mvc4-areas-and-forms-authentication
http://www.asp.net/mvc/overview/security/create-an-aspnet-mvc-5-web-app-with-email-confirmation-and-password-reset

#User and roles - i used this one. Todo remember to check it
http://typecastexception.com/post/2014/06/22/ASPNET-Identity-20-Customizing-Users-and-Roles.aspx

#unity container asp.net mvc 5 - http://debugmode.net/2014/08/28/resolving-dependencies-in-asp-net-mvc-5-using-unity-container/

#enable reshaper again - http://stackoverflow.com/questions/16364059/keyboard-shortcuts-are-not-active-in-visual-studio-2012-with-resharper-installed


##Migrations

#Enable migrations
Go to package manager console and type.
	READ THIS FIRST - http://stackoverflow.com/questions/21537558/multiple-db-contexts-in-the-same-db-and-application-in-ef-6-and-code-first-migra
	Enable-Migrations -ContextTypeName ApplicationDbContext -MigrationsDirectory Migrations\ApplicationDbContext
	Enable-Migrations -ContextTypeName TeamPOINTlessDbContext -MigrationsDirectory Migrations\TeamPOINTlessDbContext
	Add-Migration -ConfigurationTypeName TeamPOINTless.Migrations.ApplicationDbContext.Configuration Init
	Add-Migration -ConfigurationTypeName TeamPOINTless.Migrations.TeamPOINTlessDbContext.Configuration Init
	Update-Database -ConfigurationTypeName TeamPOINTless.Migrations.ApplicationDbContext.Configuration
	Update-Database -ConfigurationTypeName TeamPOINTless.Migrations.TeamPOINTlessDbContext.Configuration
	OR
	Enable-Migrations -ContextTypeName SubjectsDBContext
This command will create a new folder named Migrations.
Go to Migrations/Configuration.cs and set AutomaticMigrationsEnabled = true; doing develop. False in production.
Write the changes in Migrations/<SomeNameOrDateOrWhatever>.InitialCreate.cs
go to package manager console again and run the command: Update-Database -Verbose
You may have to go to Migrations/<SomeNameOrDateOrWhatever>.InitialCreate.cs and set: AutomaticMigrationDataLossAllowed = true;

#Add new field/Column
http://www.asp.net/mvc/overview/older-versions/getting-started-with-aspnet-mvc4/adding-a-new-field-to-the-movie-model-and-table

#Reset database table id autoincrement
	DBCC CHECKIDENT ("PAGE", RESEED, 0) 
	You fire the command bye rightclicking TeamPOINTlessDBContext in Server Explorer, and pick new query

#Routes
	http://stackoverflow.com/questions/5092589/having-issue-with-multiple-controllers-of-the-same-name-in-my-project


	DONE: #Pagination
http://gabrieleromanato.name/jquery-easy-table-pagination/
http://jsfiddle.net/gabrieleromanato/Xugej/
https://datatables.net/examples/basic_init/alt_pagination.html
http://www.asp.net/mvc/overview/getting-started/getting-started-with-ef-using-mvc/sorting-filtering-and-paging-with-the-entity-framework-in-an-asp-net-mvc-application

Cookie error - is solved when clearing browser
https://katanaproject.codeplex.com/discussions/565294
Server Error in '/' Application.


#bug
http://forums.asp.net/t/2009324.aspx?ASP+NET+Identity+signout+fails+if+inactive+for+longer+than+security+stamp+validation+interval
http://stackoverflow.com/questions/25824242/signout-is-not-working-when-calling-after-passwordsignin




#Setup typescrit unit test qunit and chuzpah test runner
	Right click test project and install chuzpah - a javascript test runner or goto Package Manager Console and type Install-Package Chuzpage. The same with Qunit.TypeScript.DefinitelyTyped
	
	Maybe we don't have to do this - Right click test project and click unload project. The edit <theprojectname>.csproj and make it look something like: 
	<ItemGroup>
    <TypeScriptCompile Include="Scripts\MyScripts\GreetingTest.ts" />
    <TypeScriptCompile Include="Scripts\typings\qunit\qunit.d.ts" />
	<!-- This is what i added-->
    <TypeScriptCompile Include="..\TypeScriptInAspNetMVCSpike\Scripts\MyScripts\**\*.ts">
      <Link>_referencesTS\Scripts\MyScripts\%(RecursiveDir)%(FileName)</Link>
    </TypeScriptCompile>
    </ItemGroup>
	Then reload project
	See : http://frankcode.net/2014/02/02/cross-project-typescript-unit-testing-part-1/ and http://frankcode.net/2014/03/16/cross-project-typescript-unit-testing-part-2/ 
	 And/or
	 Install https://chutzpah.codeplex.com/releases/view/68624 - context menu extension and Unit Test Explorer adapter.