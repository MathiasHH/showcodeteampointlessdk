// <auto-generated />
namespace TeamPOINTless.Migrations.TeamPOINTlessDbContext
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class Init : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Init));
        
        string IMigrationMetadata.Id
        {
            get { return "201411282116210_init"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
