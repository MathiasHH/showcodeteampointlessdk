using System.Web.Mvc;
using TeamPOINTless.Areas.Admin.Models;

namespace TeamPOINTless.Migrations.TeamPOINTlessDbContext
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TeamPOINTless.Areas.Admin.Models.TeamPOINTlessDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            MigrationsDirectory = @"Migrations\TeamPOINTlessDbContext";
        }

        protected override void Seed(TeamPOINTless.Areas.Admin.Models.TeamPOINTlessDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
       
            //add field/column
            //            context.Pages.AddOrUpdate(
            //                p => p.IsFrontPage,
            //                new Page { IsFrontPage = true }
            //            );

//            context.Pages.AddOrUpdate(
//                            v => v.Visible,
//                            new Page { Visible = VisibleEnum.NotVisible }
//                        );
   /*         context.Subjects.AddOrUpdate(
                v => v.Visible,
                new Subject
                {
                    Visible = VisibleEnum.NotVisible
                }
            );*/
        }
    }
}
