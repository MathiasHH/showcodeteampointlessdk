namespace TeamPOINTless.Migrations.TeamPOINTlessDbContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
     /*       CreateTable(
                "dbo.Comment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PageId = c.Int(nullable: false),
                        Position = c.Int(nullable: false),
                        IsApprovedAndThereforeVisible = c.Boolean(nullable: false),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Page", t => t.PageId, cascadeDelete: true)
                .Index(t => t.PageId);
            
            CreateTable(
                "dbo.Page",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SubjectId = c.Int(nullable: false),
                        Title = c.String(),
                        Position = c.Int(nullable: false),
                        Visible = c.Boolean(nullable: false),
                        Content = c.String(),
                        IsFrontPage = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Subject", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.Subject",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Position = c.Int(nullable: false),
                        Visible = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);*/
            
        }
        
        public override void Down()
        {
/*            DropForeignKey("dbo.Page", "SubjectId", "dbo.Subject");
            DropForeignKey("dbo.Comment", "PageId", "dbo.Page");
            DropIndex("dbo.Page", new[] { "SubjectId" });
            DropIndex("dbo.Comment", new[] { "PageId" });
            DropTable("dbo.Subject");
            DropTable("dbo.Page");
            DropTable("dbo.Comment");
        */     
        }
    }
}
