﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TeamPOINTless.Infrastructure;
using TeamPOINTless.Models;


namespace TeamPOINTless
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var factory = new CustomControllerFactory();
            ControllerBuilder.Current.SetControllerFactory(factory);



/* When getting this error : 
 * The model backing the 'ApplicationDbContext' context has changed since the database was created. Consider using Code First Migrations to update the database (http://go.microsoft.com/fwlink/?LinkId=238269).
Description: An unhandled exception occurred during the execution of the current web request. Please review the stack trace for more information about the error and where it originated in the code. 
Exception Details: System.InvalidOperationException: The model backing the 'ApplicationDbContext' context has changed since the database was created. Consider using Code First Migrations to update the database (http://go.microsoft.com/fwlink/?LinkId=238269).
Source Error: 
Line 75: 
Line 76:             // Require the user to have a confirmed email before they can log on.
Line 77:             var user = await UserManager.FindByNameAsync(model.Email);
Line 78:             if (user != null)
Line 79:             {
 
 This line works
 */
            Database.SetInitializer<ApplicationDbContext>(null);
        }
    }
}
