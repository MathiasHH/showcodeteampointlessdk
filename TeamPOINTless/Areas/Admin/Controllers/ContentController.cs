﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamPOINTless.Areas.Admin.Models;

namespace TeamPOINTless.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ContentController : Controller
    {

        private readonly TeamPOINTlessDbContext _db = new TeamPOINTlessDbContext();
        // GET: Admin/Content
        public ActionResult Index()
        {
            return View();
        }

        // GET: Admin/Content
        public ActionResult Content()
        {
            ViewBag.SubjectsList = _db.Subjects.Count();
            ViewBag.NumberOfPages = _db.Pages.Count();
            return View("Content", _db.Subjects.ToList()); 
        }
    }
}