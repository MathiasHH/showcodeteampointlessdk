﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TeamPOINTless.Areas.Admin.Models;

namespace TeamPOINTless.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SubjectController : Controller
    {
        private TeamPOINTlessDbContext db = new TeamPOINTlessDbContext();
        private readonly ISubjectRepository _subjectRepository;

        // GET: Admin/Subject
        public SubjectController(ISubjectRepository subjectRepository)
        {
            this._subjectRepository = subjectRepository;
        }

        public ActionResult Index()
        {
            return RedirectToAction(actionName: "Content", controllerName: "Content");//We dont want to use Index to admin content
        }

        // GET: Admin/Subject/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject subject = _subjectRepository.FindById(id);
//            Subject subject = db.Subjects.Find(id);
            if (subject == null)
            {
                return HttpNotFound();
            }
            return View(subject);
        }

        public ActionResult ForMangeEmner()
        {
            return View("TooManySubjects");
        }


        // GET: Admin/Subject/Create
        public ActionResult Create()
        {
            if (db.Subjects.Count() > 5)
            {
                return RedirectToAction("ForMangeEmner");
            }
            return View();
        }

        // POST: Admin/Subject/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,Position,Visible")] Subject subject)
        {
            if (ModelState.IsValid)
            {
                db.Subjects.Add(subject);
                db.SaveChanges();
                return RedirectToAction(actionName: "Content", controllerName: "Content");
            }

            return View(subject);
        }

        // GET: Admin/Subject/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject subject = db.Subjects.Find(id);
            if (subject == null)
            {
                return HttpNotFound();
            }
            return View(subject);
        }

        // POST: Admin/Subject/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,Position,Visible")] Subject subject)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subject).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(subject);
        }

        // GET: Admin/Subject/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject subject = db.Subjects.Find(id);
            if (subject == null)
            {
                return HttpNotFound();
            }
            return View(subject);
        }

        // POST: Admin/Subject/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Subject subject = db.Subjects.Find(id);
            db.Subjects.Remove(subject);
            db.SaveChanges();
            return RedirectToAction(actionName: "Content", controllerName: "Content");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
