﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TeamPOINTless.Areas.Admin.Models;

namespace TeamPOINTless.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PageController : Controller
    {
        private TeamPOINTlessDbContext db = new TeamPOINTlessDbContext();

        // GET: Admin/Page
        public ActionResult Index()
        {
            return View(db.Pages.ToList());
        }

        // GET: Admin/Page/Details/5
        public ActionResult Details(int? id) // int? - is a int or null - It is a shorthand for Nullable<int>. A nullable type can represent the normal range of values for its underlying value type, plus an additional null value. For example, a Nullable<Int32>, pronounced "Nullable of Int32," can be assigned any value from -2147483648 to 2147483647, or it can be assigned the null value. 
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = db.Pages.Find(id);
            if (page == null)
            {
                return HttpNotFound();
            }
            return View(page);
        }

        // GET: Admin/Page/Create

        

        public ActionResult Create(int? id, string title = "")
        {
            if (id > 0)
            {
                ViewData["TheSubjectID"] = id;
                ViewData["TheSubjectTitle"] = title; 
            }
            return View();
        }

        // POST: Admin/Page/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SubjectId,Title,Position,Visible,Content,IsFrontPage,CreatedDateTime,UpdatedDateTime,EventDateTime,ShowEventDateTimeOnFrontPage,FlickrAccountId,FlickrAlbumName")] Page page)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                //if page has is Frontpage is true
                if ( page.IsFrontPage)
                {
                    FindOldFrontPage(page);
                }
                //if page has is next event is true
                if ( page.ShowEventDateTimeOnFrontPage)
                {
                    FindOldNextEvent(page);
                }
                page.CreatedDateTime = DateTime.Now;
                page.UpdatedDateTime = null;

                db.Pages.Add(page);
                db.SaveChanges();
                return RedirectToAction(actionName: "Content", controllerName: "Content");
            }

            return View(page);
        }

        private void FindOldFrontPage(Page page)
        {
            //http://stackoverflow.com/questions/25454801/icollectionview-throw-entity-framework-attach-exception
            Page oldFrontPage = db.Pages.AsNoTracking().FirstOrDefault(x => x.IsFrontPage == true);
            if (oldFrontPage == null) return;
            if (oldFrontPage.Id == page.Id) return; // return if its the same page
            SetIsFrontPageFalseAndSaveToDb(oldFrontPage);
        }

        private void SetIsFrontPageFalseAndSaveToDb(Page oldFrontPage)
        {
            oldFrontPage.IsFrontPage = false;
            db.Entry(oldFrontPage).State = EntityState.Modified;
            db.SaveChanges();
        }

        private void FindOldNextEvent(Page page)
        {
            //http://stackoverflow.com/questions/25454801/icollectionview-throw-entity-framework-attach-exception
            Page oldNextEvent = db.Pages.AsNoTracking().FirstOrDefault(x => x.ShowEventDateTimeOnFrontPage == true);
            if (oldNextEvent == null) return;
            if (oldNextEvent.Id == page.Id) return; // return if its the same page
            SetIsNextEventFalseAndSaveToDb(oldNextEvent);
        }

        private void SetIsNextEventFalseAndSaveToDb(Page oldNextEvent)
        {
            oldNextEvent.ShowEventDateTimeOnFrontPage = false;
            db.Entry(oldNextEvent).State = EntityState.Modified;
            db.SaveChanges();
        }

    // GET: Admin/Page/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = db.Pages.Find(id);
            if (page == null)
            {
                return HttpNotFound();
            }
            return View(page);
        }

        // POST: Admin/Page/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SubjectId,Title,Position,Visible,Content,IsFrontPage,CreatedDateTime,UpdatedDateTime,EventDateTime,ShowEventDateTimeOnFrontPage,FlickrAccountId,FlickrAlbumName")] Page page)
        {
            if (ModelState.IsValid)
            {
                if (page.IsFrontPage)
                {
                    FindOldFrontPage(page);
                }
                if (page.ShowEventDateTimeOnFrontPage)
                {
                    FindOldNextEvent(page);
                }
                if (page.EventDateTime.Equals(null))
                {
                    page.ShowEventDateTimeOnFrontPage = false;
                }
                page.UpdatedDateTime = DateTime.Now;
                db.Entry(page).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction(actionName:"Content", controllerName:"Content");
            }
            return View(page);
        }

        // GET: Admin/Page/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = db.Pages.Find(id);
            if (page == null)
            {
                return HttpNotFound();
            }
            return View(page);
        }

        // POST: Admin/Page/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Page page = db.Pages.Find(id);
            db.Pages.Remove(page);
            db.SaveChanges();
            return RedirectToAction(actionName: "Content", controllerName: "Content");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
