﻿
namespace TeamPOINTless.Areas.Admin.Models
{
    public interface ISubjectRepository
    {
        Subject FindById(int? id);
    }
}

