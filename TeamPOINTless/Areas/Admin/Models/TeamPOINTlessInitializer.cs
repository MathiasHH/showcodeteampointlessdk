﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamPOINTless.Areas.Admin.Models
{
    public class TeamPOINTlessInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<TeamPOINTlessDbContext>
    {
        /*The Seed method takes the database context object as an input parameter, and the code in the method uses
        that object to add new entities to the database. For each entity type, the code creates a collection of new
        entities, adds them to the appropriate DbSet property, and then saves the changes to the database. It isn't
        necessary to call the SaveChanges method after each group of entities, as is done here, but doing that helps
        you locate the source of a problem if an exception occurs while the code is writing to the database.*/
//        protected override void Seed(TeamPOINTlessDbContext context)
//        {
//            var subjects = new List<Subject>
//            {
//            new Subject{Title="BIF",Position= -5,Visible= VisibleEnum.VisibleForEveryone},
//            new Subject{Title="Aab",Position= 0,Visible=VisibleEnum.VisibleForEveryone},
//            new Subject{Title="OB",Position= 5,Visible=VisibleEnum.VisibleForEveryone},
//            new Subject{Title="FCK",Position= 10,Visible=VisibleEnum.VisibleForEveryone},
//            };
//
//            subjects.ForEach(s => context.Subjects.Add(s));
//            context.SaveChanges();
//
//            var pages = new List<Page>
//            {
//            new Page{SubjectId=1,Title = "Vi er vestegnen", Position = 1, Visible = VisibleEnum.VisibleForEveryone, Content = "Vi er vesterne, vesterne, vesterne, versterne, aaaalaeee, vi er vestegnen"},
//            new Page{SubjectId=1,Title = "FC blod på trøjen", Position = 2, Visible = VisibleEnum.VisibleForEveryone, Content = "FC blod på trøjen, sov med den om natten, den er bare rød så rød, den er fyldt af fc blod."},
//            
//            new Page{SubjectId=2,Title = "Aab yeah yeah yeah", Position = 1, Visible = VisibleEnum.VisibleForEveryone, Content = "Aab yeah yeah yeah. Aab yeah yeah yeah."},
//            
//            new Page{SubjectId=3,Title = "OB OB", Position = 1, Visible = VisibleEnum.VisibleForEveryone, Content = "OB OB OB OB OB OB OB OB OB ."},
//            
//            new Page{SubjectId=3,Title = "OB vinder gul i år", Position = 5, Visible = VisibleEnum.VisibleForEveryone, Content = "OB vinder guld i år med røde lygter og tjald"},
//            
//            new Page{SubjectId=4,Title = "Vi er FCK", Position = 1, Visible = VisibleEnum.VisibleForEveryone, Content = "Vi er FCK Vi er FCK Vi er FCK ."},
//            };
//            pages.ForEach(p => context.Pages.Add(p));
//            context.SaveChanges();
//        }
    }
}