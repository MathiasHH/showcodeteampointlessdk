﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TeamPOINTless.Areas.Admin.Models
{
    public class Page
    {
        public int Id { get; set; }

        [DisplayName("Emne ID")]
        public int SubjectId { get; set; }

        [Required]
        [DisplayName("Titel")]
        public string Title { get; set; }

        [DisplayName("Oprettet")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")] 
        public DateTime? CreatedDateTime { get; set; }

        [DisplayName("Opdateret")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? UpdatedDateTime { get; set; }

        [DisplayName("Event dato")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? EventDateTime { get; set; }

        [DisplayName("Vis event dato på forside")]
        public bool ShowEventDateTimeOnFrontPage { get; set; }

        public int Position { get; set; }

        [DisplayName("Synlig")]
        public VisibleEnum Visible { get; set; }
//        [DataType(DataType.MultilineText)]

        [AllowHtml]
        [DisplayName("Brødtekst")]
        public string Content { get; set; }

        [DisplayName("Er forside")]
        public bool IsFrontPage { get; set; }

        [DisplayName("Flickr konto id")]
        public string FlickrAccountId { get; set; }

        [DisplayName("Flickr album navn")]
        public string FlickrAlbumName { get; set; }

        //not implemented yet - TODO implement it 
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
