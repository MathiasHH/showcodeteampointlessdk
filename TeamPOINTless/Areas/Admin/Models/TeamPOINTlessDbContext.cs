﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace TeamPOINTless.Areas.Admin.Models
{
    // ReSharper disable once InconsistentNaming
    public class TeamPOINTlessDbContext : DbContext
    {
        public TeamPOINTlessDbContext()
            : base("TeamPOINTlessDbContext")
        {
          
        }

        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
