﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamPOINTless.Areas.Admin.Models
{
    public class Subject
    {
        public int ID { get; set; }
        [Required]
        public string Title { get; set; }
        public int Position { get; set; }
        //public bool Visible { get; set; }
        public VisibleEnum Visible { get; set; }

        public virtual ICollection<Page> Pages { get; set; }

 
    }
}
