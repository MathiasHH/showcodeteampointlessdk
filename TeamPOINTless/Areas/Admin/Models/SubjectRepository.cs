﻿using System;

namespace TeamPOINTless.Areas.Admin.Models
{
    public class SubjectRepository : ISubjectRepository
    {
        private readonly TeamPOINTlessDbContext _teamPointlessDbContext = new TeamPOINTlessDbContext();

        public Subject FindById(int? id)
        {
            return _teamPointlessDbContext.Subjects.Find(id);
        }
    }
}