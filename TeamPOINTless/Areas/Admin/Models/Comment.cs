﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamPOINTless.Areas.Admin.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public int PageId { get; set; }
        public int Position { get; set; }
        public bool IsApprovedAndThereforeVisible { get; set; }
        public string Content { get; set; }

        //There should be a relation to the page
        //one or more comments belong to a single page
        //Todo create relation
    }
}
