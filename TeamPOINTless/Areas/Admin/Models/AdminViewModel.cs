﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TeamPOINTless.Areas.Admin.Models
{
    public class RoleViewModel
    {
        public string Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "RoleName")]
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class EditUserViewModel
    {
        //Todo why is the Id a string?
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        public bool IsApproved { get; set; }

        public IEnumerable<SelectListItem> RolesList { get; set; }

        public bool EmailConfirmed { get; set; }

//        public bool Admin { get; set; }
    }
}