﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamPOINTless.Areas.Admin.Models
{
    public enum VisibleEnum
    {
        NotVisible,
        VisibleForAdmins,
        VisibleForUsers,
        VisibleForEveryone
    }
}