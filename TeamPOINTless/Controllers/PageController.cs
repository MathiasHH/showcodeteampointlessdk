﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TeamPOINTless.Areas.Admin.Models;
using TeamPOINTless.Models;

namespace TeamPOINTless.Controllers
{
    public class PageController : Controller
    {
        private TeamPOINTlessDbContext db = new TeamPOINTlessDbContext();

        // GET: Page
        [AllowAnonymous]
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = db.Pages.Find(id);
            if (page == null)
            {
                return HttpNotFound();
            }

            if (page.Visible == VisibleEnum.NotVisible)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            var boolean1 = User.IsInRole("Admin");
            var bo = boolean1;
            //if not logged in and page is only visible for admins
            if (page.Visible == VisibleEnum.VisibleForAdmins && !User.IsInRole("Admin"))
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            //if not logged in and page is only visible for users
            if (page.Visible == VisibleEnum.VisibleForUsers && !User.Identity.IsAuthenticated)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }

            if (page.Title.ToLower().Equals("offentlig chat") ||
                page.Title.ToLower().Equals("chat for medlemmer") ||
                page.Title.ToLower().Equals("chat for admins"))
            {
//                return View(viewName: "Chat");
                return View(viewName: "Chat", model: page);
            }

            ViewBag.LastFivePages = new LatestPages().LastNoOfPages(7);
            
            return View(page);
        }
    }
}