﻿using System;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Threading;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using TeamPOINTless.Areas.Admin.Models;
using TeamPOINTless.Models;

namespace TeamPOINTless.Controllers
{
    public class HomeController : Controller
    {
        private readonly TeamPOINTlessDbContext _db = new TeamPOINTlessDbContext();

        [AllowAnonymous]
        public ActionResult Index()
        {
            Page nextEvent = _db.Pages.FirstOrDefault(x => x.ShowEventDateTimeOnFrontPage == true);
            if (nextEvent != null)
            {
                //Todo create viewmodel instead of using viewbagg
                
                ViewBag.nextEvent = nextEvent;

                //We need the english format for the jquery module that counts down         
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
//                Set the string to the english format
                DateTime? dateTime = nextEvent.EventDateTime;
                ViewBag.EventtimeString = dateTime.ToString();
//                set it back to danish
                Thread.CurrentThread.CurrentCulture = new CultureInfo("da-DK");
            }
            Page frontpage = _db.Pages.FirstOrDefault(x => x.IsFrontPage == true);

            ViewBag.LastFivePages = new LatestPages().LastNoOfPages(7);

            return View(frontpage);
        }

    }
}