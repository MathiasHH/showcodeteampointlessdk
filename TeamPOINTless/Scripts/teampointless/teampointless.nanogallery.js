﻿/**
 * We use Nano Gallery for galleries
 * See http://nanogallery.brisbois.fr/
 */

(function () {
    "use strict";

    if (typeof flickrAccountId === 'undefined') {
        return;
    }

    //a flickr accountid is 13
    if (flickrAccountId.length === 13) {
        $("#nanoGallery1").nanoGallery({
            kind: 'flickr',
            //                        userID: '130577458@N02',
            userID: flickrAccountId,

            //pick single album
            //id for the album Pixi
            //        album: '72157650747850269',// Dosn't work
            //  instead you could whitelist the name of a single album.
            // if wrong name no album is shown
            // if part of name is written album is shown. For example if album name is Pixi then Pix will show album but Pixi1 and Pixo will not show album. None at all.
            //If name is an empty string all albums will be shown.
            whiteList: whitelistName,

            thumbnailWidth: 'auto',
            thumbnailHeight: 250,
            thumbnailHoverEffect: [{ name: 'labelAppear75', duration: 300 }],
            thumbnailGutterWidth: 0,
            thumbnailGutterHeight: 0,
            colorScheme: 'dark',
            thumbnailLabel: { display: true, position: 'overImageOnMiddle', align: 'center' }

        });
    }

}());