﻿/**
  * This script is used to format the event on the frontpage
  * it uses jQuery to format the data
  * 
  */

(function () {

    //create break in time
    if ($(window).width() < 520) {
        if ($('#break-before-clock').length <= 0)//if id break-before dosn't exist
        {
            $('#clock').before('<span id="break-before-clock"><br /></span>');
        }
    }
    //remove break in time
    $(window).resize(function () {
        var windowWidth = $(window).width();
        if (windowWidth >= 520) {
            if ($('#break-before-clock').length > 0) {
                $('#break-before-clock').remove();
            }
        }
        //create break in time
        if (windowWidth < 520) {
            if ($('#break-before-clock').length <= 0) {
                $('#clock').before('<span id="break-before-clock"><br /></span>');
            }
        }
  
    });
}());