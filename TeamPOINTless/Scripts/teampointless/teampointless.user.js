/// <reference path="../typings/jquery/jquery.d.ts"/>
var User = (function () {
    function User(ec) {
        this.emailConfirmed = typeof ec !== "undefined" ? ec : false;
    }
    User.prototype.hideIfEmailIsConfirmed = function () {
        if (this.emailConfirmed.toString() === "True") {
            $("#email-confirmed").hide(0, "swing", null);
        }
    };
    return User;
})();
(function init() {
    $(document).ready(function () {
        var ec = typeof EMAILCONFIRMED != 'undefined' ? EMAILCONFIRMED : false;
        new User(ec).hideIfEmailIsConfirmed();
    });
})();
//# sourceMappingURL=teampointless.user.js.map