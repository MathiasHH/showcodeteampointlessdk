﻿/**
  * Script to wire up your TinyMCE editor
  * 
  */

(function () {
    $(document).ready(function () {

        // Initialize your tinyMCE Editor with your preferred options
        tinyMCE.init({
            // General options
            mode: "textareas",
            height: 600,
            width: 750,
            resize: "both",
            theme: "modern",
            plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern"],
            toolbar1: "styleselect | bold | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent nonbreaking | undo redo",
            toolbar2: "formatselect fontsizeselect fontselect forecolor backcolor removeformat hr charmap | cut copy paste pastetext pasteword",
            toolbar3: "image media link unlink table | code | preview fullscreen print | visualchars searchreplace | emoticons",
            image_advtab: true,
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            theme_advanced_statusbar_location: "bottom",
            theme_advanced_resizing: true,

            // Example content CSS (should be your site CSS)
            content_css: "Content/Site.css",
        });

    });
}());