﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
// tell the compiler the global variable EMAILCONFIRMED has been declared somewhere
declare var EMAILCONFIRMED: boolean;

class User {

    emailConfirmed: boolean;

    constructor(ec: boolean) {
        this.emailConfirmed = typeof ec !== "undefined" ? ec : false;
    }

    hideIfEmailIsConfirmed() {
        if (this.emailConfirmed.toString() === "True") {
            $("#email-confirmed").hide(0, "swing", null);
        }
    }
}

(function init() {
    $(document).ready(function () {
        var ec = typeof EMAILCONFIRMED != 'undefined' ? EMAILCONFIRMED : false;
        new User(ec).hideIfEmailIsConfirmed();
    });
})();
  
    
 
 
