﻿/**
  * This script use regular expression to search in a HTML Table and return a table row
  * It's a fork of the snippet this guy, http://stackoverflow.com/users/949476/dfsq created, see http://stackoverflow.com/questions/9127498/how-to-perform-a-real-time-search-and-filter-on-a-html-table
  * 
  */

(function() {
    $(document).ready(function() {
        var $rows = $('.table tr');
        $rows = $rows.slice(1); //We don't want to remove the first (header) row
        $('#search').keyup(function() {

            var val = '^(?=.*\\b' + $.trim($(this).val()).split(/\s+/).join('\\b)(?=.*\\b') + ').*$',
                reg = RegExp(val, 'i'),
                text;

            $rows.show().filter(function() {
                text = $(this).text().replace(/\s+/g, ' ');
                return !reg.test(text);
            }).hide();
        });
    });
}());