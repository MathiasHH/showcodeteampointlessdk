﻿/**
 *  This script just tell the user to not use Google chrome when creating and editing content. 
 * see http://www.asp.net/mvc/overview/getting-started/introduction/examining-the-edit-methods-and-edit-view
 * TODO - write code so it works with google chrome and delete this script
 */

(function () {
    if (/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
        alert('This is Chrome. There is a bug in Chrome (see http://www.asp.net/mvc/overview/getting-started/introduction/examining-the-edit-methods-and-edit-view) so use Firefox, Safari, Internet Explorer or whatever to create content.');
        //            $('.btn btn-default pik-chrome').hide();
        $('.pik-chrome').hide();
        $('.pik-chrome').replaceWith("<h2 style=background-color:RED>There is a bug in Chrome (see http://www.asp.net/mvc/overview/getting-started/introduction/examining-the-edit-methods-and-edit-view) so no save button. Use Firefox, Safari, Internet Explorer or whatever to create content.</h2>");
    }
}());
  