﻿/**
  * This script is used for the countdown
  * The chat view uses jquery.countdown.min.js
  *
  */
(function () {
    var sixMonths = nextEventDate.theDate;//new Date(new Date().valueOf() + 6 * 30 * 24 * 60 * 60 * 1000);
        $('#clock').countdown(sixMonths).on('update.countdown', function (event) {
            var $this = $(this).html(event.strftime(''
                + '<span>%-w</span> uge%!w '
                + '<span>%-d</span> dage '
                + '<span>%H</span> timer '
                + '<span>%M</span> min '
                + '<span>%S</span> sek'));
        });

}());