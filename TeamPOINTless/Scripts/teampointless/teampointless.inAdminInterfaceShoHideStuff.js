﻿/**
  * This script is used when creating or editing a page
  * It uses jQuery 
  * to show and hiding data
  * to hide some divs when title is Chat
  * 
  */

(function () {
    var eventishidden;

    var hideAndReset = function () {
        eventishidden = true;
        $(".if-chat-then-hide").hide();
        $('#checkboxIFP').prop('checked', false);
        $('#show-event-date-frontpage').prop('checked', false);
        $('#eventdate-editor input').val('');
        $('#eventdate-editor').hide();
        $('#eventdate-checkbox').prop('checked', false);
        $('#eventdate-checkbox').hide();
        $('#eventdate-container').hide();
        var my_editor_id = 'Content'; //tinymce.editors[0].id
        if (tinymce.get(my_editor_id) !== null) {
            // set the content empty
            tinymce.get(my_editor_id).setContent('');
        }
        $('#flickr-accountid input').val('');
        $('#flickr-albumname input').val('');
    }
    var show = function () {
        eventishidden = false;
        $('#eventdate-container').show();
        $('#eventdate-checkbox').show();
        $('#eventdate-editor').show();
        $(".if-chat-then-hide").show();
    }

    /*Chat*/
    $(document).ready(function () {
        if ($('#titletext').val().toLowerCase() === 'offentlig chat' ||
            $('#titletext').val().toLowerCase() === 'chat for medlemmer' ||
            $('#titletext').val().toLowerCase() === 'chat for admins') {
            hideAndReset();
        }

        $('#titletext').keyup(function () {
            if ($('#titletext').val().toLowerCase() === 'offentlig chat' ||
                $('#titletext').val().toLowerCase() === 'chat for medlemmer' ||
                $('#titletext').val().toLowerCase() === 'chat for admins') {
                hideAndReset();
            }
            else {
                if (eventishidden === true) {
                    show();
                }
            }
        });
    });




    /*
     * You could use some of this if you want to hide event for some reason. if not delete it
     */
    //    var hideShowEventDateTimeOnFrontPage = function () {
    //        console.log("HIDE - hideShowEventDateTimeOnFrontPage()  called");
    //        $('#show-event-date-frontpage').hide();
    //        $('#show-event-date-frontpage-label').hide();
    //    }
    //    var showShowEventDateTimeOnFrontPage = function () {
    //        console.log("SHOW - showShowEventDateTimeOnFrontPage()  called");
    //        $('#show-event-date-frontpage').show();
    //        $('#show-event-date-frontpage-label').show();
    //    }
    //if there is no a eventdatemodel when page loads
    //    if (!EventDateTimeModel) {
    //        $('#eventdate-checkbox').prop('checked', false);
    //        $('#eventdate-editor').hide();
    //        $('#eventdate-container').hide();
    //        hideShowEventDateTimeOnFrontPage();
    //    }
    //    else if (EventDateTimeModel) {
    //        $('#eventdate-checkbox').prop('checked', true);
    //    }
    //    //when page loads
    //    var selectedValue = $('#visiblevalue').find('option:selected').val();
    //    if (selectedValue === "0" || selectedValue === "1") { 
    //        $('#isFrontPage').hide();
    //        $('#eventdate-container').hide();
    //    }
    //    else {
    //        $('#isFrontPage').show();
    //        $('#eventdate-container').show();
    //    }
    //  
    //when click/touch
    //    $('#eventdate-checkbox').click(function () {
    //        if ($('#eventdate-checkbox').is(':checked')) {
    //            $('#eventdate-editor').show();
    //            var content = $('#eventdate-editor input').val();
    //            if (content.length > 0) {
    //                showShowEventDateTimeOnFrontPage();
    //            }
    //        } else {
    //            $('#eventdate-editor input').val('');
    //            $('#eventdate-editor').hide();
    //            $('#show-event-date-frontpage').prop('checked', false);
    //            hideShowEventDateTimeOnFrontPage();
    //        }
    //    });
    //    $('#eventdate-editor input').keyup(function () {
    //        var content = $('#eventdate-editor input').val();
    //        if (content.length > 0) {
    //            showShowEventDateTimeOnFrontPage();
    //        } else if (content.length === 0) {
    //            $('#show-event-date-frontpage').prop('checked', false);
    //            hideShowEventDateTimeOnFrontPage();
    //        }
    //    });


    //
    //    if (!isFrontPage || isFrontPage === "False") {
    //        $('#checkboxIFP').prop('checked', false);
    //    }

    //when click/touch
    //    $("#visiblevalue").change(function () {
    //        var selectedValue = $(this).find('option:selected').val();
    //        //0 = NotVisible. 1 = VisibleForAdmin
    //        if (selectedValue === "0" || selectedValue === '1') {
    //            $('#checkboxIFP').prop('checked', false);
    //            $('#isFrontPage').hide();
    //            $('#eventdate-container').hide();
    //            $('#show-event-date-frontpage').prop('checked', false);
    //            $('#eventdate-editor input').val('');
    //            $('#eventdate-checkbox').prop('checked', false);
    //        } else {
    //            $('#isFrontPage').show();
    //            $('#eventdate-container').show();
    //        }
    //    });
}());