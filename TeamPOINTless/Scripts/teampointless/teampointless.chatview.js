﻿/**
  * This script is used for the chat view
  * The chat view uses SignalR
  * http://www.asp.net/signalr/overview/getting-started/tutorial-getting-started-with-signalr
  */
(function () {
    $(function () {
        // Reference the auto-generated proxy for the hub.
        var chat = $.connection.chatHub;
        // Create a function that the hub can call back to display messages.
        chat.client.addNewMessageToPage = function (name, message) {
            // Add the message to the page.
            $('#discussion').append('<li><strong>' + htmlEncode(name)
                + '</strong>: ' + htmlEncode(message) + '</li>');
        };
     // Get the user name and store it to prepend to messages.
        if (username == "" || username == null) {
            username = prompt("Please enter your name", "");
            while (username == "" || username == null) {
                username = prompt("Please enter your name", "");
            }
        }
        $('#displayname').val(username);
        // Set initial focus to message input box.
        $('#message').focus();
        // Start the connection.
        $.connection.hub.start().done(function () {
            $('#message').keydown(function (event) {
                if (event.which === 13) {
                    send();
                }
            });
            $('#sendmessage').click(function () {
                send();
            });
            $('#clearMessages').click(function () {
                if (confirm("clear text?")) {
                    $('#discussion').empty();
                }
            });
        });

        var send = function () {
            if ($('#message').val().length > 0) {
                // Call the Send method on the hub.
                chat.server.send($('#displayname').val(), $('#message').val());
                // Clear text box and reset focus for next comment.
                $('#message').val('').focus();
            }
        }
    });

    // This optional function html-encodes messages for display in the page.
    function htmlEncode(value) {
        var encodedValue = $('<div />').text(value).html();
        return encodedValue;
    }
}());