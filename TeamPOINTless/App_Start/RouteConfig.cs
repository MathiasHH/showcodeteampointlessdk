﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TeamPOINTless
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //Admin/Page/Create
//            routes.MapRoute(
//                "area_default",
//                "{area}/{controller}/{action}/{id}",
//                new { controller = "Page", action = "Create", id = UrlParameter.Optional }
//                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces:new[] { "TeamPOINTless.Controllers" }
            );

        }
    }
}
