﻿using System.Web;
using System.Web.Mvc;

namespace TeamPOINTless
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //Protect the Application with Authorize Attribute
            filters.Add(new System.Web.Mvc.AuthorizeAttribute());
            //Protect the Application with SSL
            filters.Add(new RequireHttpsAttribute());
        }
    }
}
