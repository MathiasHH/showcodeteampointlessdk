﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using TeamPOINTless.Areas.Admin.Models;
namespace TeamPOINTless.Models
{

    public class LatestPages
    {
        private readonly TeamPOINTlessDbContext _db = new TeamPOINTlessDbContext();

        /**
         * This method takes the X number of newest pages. 
         */
        public ICollection<Page> LastNoOfPages(int number)
        {
            List<Page> lastUpdatedPages = _db.Pages.Where(x => x.Visible != VisibleEnum.VisibleForAdmins).OrderByDescending(x => x.UpdatedDateTime).Take(number).ToList();

            List<Page> lastCreatedPages = _db.Pages.Where(x => x.Visible != VisibleEnum.VisibleForAdmins).OrderByDescending(x => x.CreatedDateTime).Take(number).ToList();

            //We use the UpdatedDateTime property for sorting. So if a page has not been updated we use the create time
            for (int i = 0; i < lastCreatedPages.Count; i++)
            {
                if (lastCreatedPages.ElementAt(i).UpdatedDateTime.Equals(null))
                {
                    lastCreatedPages.ElementAt(i).UpdatedDateTime = lastCreatedPages.ElementAt(i).CreatedDateTime;
                }
            }
            
            List<Page> totallist = lastUpdatedPages.Concat(lastCreatedPages).ToList();
            totallist = totallist.OrderByDescending(x => x.UpdatedDateTime).ToList();
            totallist = totallist.Distinct().ToList();
            totallist = totallist.Take(number).ToList();
            return totallist;
        }

    }
}
